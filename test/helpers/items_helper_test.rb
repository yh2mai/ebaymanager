require 'test_helper'

class ItemsHelperTest < ActionView::TestCase
  test "get string between tag" do
    body = "tag1bodytag2"
    tag1 = "tag1"
    tag2 = "tag2"
    assert_equal getstringbetweentags(body, tag1, tag2) , "body"
  end
end