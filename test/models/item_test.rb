require 'test_helper'

class ItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user = users(:michael)
    @item = @user.items.build()
  end
  
  test "should be valid" do
    assert @item.valid?
  end
  
  test "user_id should be present" do
    @item.user_id = nil;
    assert_not @item.valid?
  end
end
