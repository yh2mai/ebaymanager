require 'test_helper'

class UnavailableDatesControllerTest < ActionController::TestCase
  setup do
    @unavailable_date = unavailable_dates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:unavailable_dates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create unavailable_date" do
    assert_difference('UnavailableDate.count') do
      post :create, unavailable_date: { ebayID: @unavailable_date.ebayID, unavailable_date: @unavailable_date.unavailable_date }
    end

    assert_redirected_to unavailable_date_path(assigns(:unavailable_date))
  end

  test "should show unavailable_date" do
    get :show, id: @unavailable_date
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @unavailable_date
    assert_response :success
  end

  test "should update unavailable_date" do
    patch :update, id: @unavailable_date, unavailable_date: { ebayID: @unavailable_date.ebayID, unavailable_date: @unavailable_date.unavailable_date }
    assert_redirected_to unavailable_date_path(assigns(:unavailable_date))
  end

  test "should destroy unavailable_date" do
    assert_difference('UnavailableDate.count', -1) do
      delete :destroy, id: @unavailable_date
    end

    assert_redirected_to unavailable_dates_path
  end
end
