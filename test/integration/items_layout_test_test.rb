require 'test_helper'

class ItemsLayoutTestTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "items_layout after log in" do
    @user = users(:michael)
    log_in_as(@user)
    get items_path
    assert_template 'items/index'
  end
end