require 'test_helper'

class AddItemsTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:michael)
  end
  
  test "invalid if item has been added" do
    log_in_as(@user, remember_me: '1')
    assert_no_difference 'Item.count' do
      get add_from_SYW_path, {syw_id: "1230", title: ""}
    end
    assert_not flash.empty?
  end
  
end
