require 'test_helper'

class ItemsCreateTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  
  test "created item with add_item get" do
    @user = users(:michael)
    @SYWID = "foo"
    assert_difference 'Item.count', 1 do 
      @path = add_item_path + "?user_id=" + @user.id.to_s + "&SYWID=" + @SYWID
      get @path
    end
    assert_equal @user.id, Item.last.user_id
    assert_equal @SYWID, Item.last.SYWID
  end
end
