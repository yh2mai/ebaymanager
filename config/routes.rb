Rails.application.routes.draw do
  resources :unavailable_dates
  get 'sessions/new'
  
  root 'static_pages#home'
  get 'help'  => 'static_pages#help'
  get 'about' => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'signup' => 'users#new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  delete 'violated_brand' => 'users#destroy_brand'
  get 'add_violated_brand' => 'users#add_violated_brand'
  get 'add_item' => 'items#add'
  post 'update_price' => 'items#update_price'
  post 'add_from_SYW' => 'items#add_from_SYW'
  post 'clean' => 'items#clean'
  get 'add_from_SYW' => 'items#add_item'
  post 'add_list_from_SYW' => 'items#add_list_from_SYW'
  get 'query_result' => 'items#query'
  post 'update_description' => 'items#update_description'
  delete 'items' => 'items#destroy'
  resources :users
  resources :items
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
