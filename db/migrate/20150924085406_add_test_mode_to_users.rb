class AddTestModeToUsers < ActiveRecord::Migration
  def change
    add_column :users, :test_mode, :boolean, default: false
  end
end
