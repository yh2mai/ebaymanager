class AddCurrentNumToUsers < ActiveRecord::Migration
  def change
     add_column :users, :current_num, :int, default: 0
  end
end
