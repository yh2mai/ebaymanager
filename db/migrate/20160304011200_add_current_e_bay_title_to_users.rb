class AddCurrentEBayTitleToUsers < ActiveRecord::Migration
  def change
      add_column :users, :currentEBayTitle, :string, default: ""
      remove_column :items, :currentEBayTitle, :string
  end
end
