class AddIndexToItemsEbayid < ActiveRecord::Migration
  def change
     add_index :items, :ebayID, unique: true
  end
end
