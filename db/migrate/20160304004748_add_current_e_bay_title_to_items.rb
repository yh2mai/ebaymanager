class AddCurrentEBayTitleToItems < ActiveRecord::Migration
  def change
    add_column :items, :currentEBayTitle, :string, default: ""
  end
end
