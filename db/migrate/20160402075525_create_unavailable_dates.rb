class CreateUnavailableDates < ActiveRecord::Migration
  def change
    create_table :unavailable_dates do |t|
      t.string :ebayID
      t.string :unavailable_date

      t.timestamps null: false
    end
  end
end
