class AddStoreUrlAndIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :store_URL, :string, default: ""
    add_column :users, :store_ID, :string, default: ""
  end
end
