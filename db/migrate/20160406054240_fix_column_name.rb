class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :unavailable_dates, :ebayID, :ebayid
  end
end
