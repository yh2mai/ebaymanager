class AddViolatedBrandsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :violated_brands, :string, array: true, default: []
  end
end
