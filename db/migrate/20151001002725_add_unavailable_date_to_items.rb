class AddUnavailableDateToItems < ActiveRecord::Migration
  def change
    add_column :items, :unavailable_date, :string, default: ""
  end
end
