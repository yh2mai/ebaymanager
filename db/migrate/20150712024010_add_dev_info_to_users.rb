class AddDevInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :devID, :string, default: ""
    add_column :users, :appID, :string, default: ""
    add_column :users, :certID, :string, default: ""
    add_column :users, :ebayToken, :string, default: ""
    add_column :users, :numStock, :integer, default: 3
    add_column :users, :SYWToken, :string, default: ""
    add_column :users, :SYWHash, :string, default: ""
  end
end
