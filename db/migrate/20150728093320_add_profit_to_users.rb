class AddProfitToUsers < ActiveRecord::Migration
  def change
     add_column :users, :profit, :float, default: 3
  end
end
