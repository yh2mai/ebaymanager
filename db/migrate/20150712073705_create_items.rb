class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :SYWID
      t.float :SYWPrice
      t.boolean :SYWAvailability
      t.string :ebayID
      t.string :ebayTitle
      t.integer :quantity
      t.float :fixed_price
      t.string :category
      t.string :thumb_url
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :items, [:user_id, :created_at]
  end
end
