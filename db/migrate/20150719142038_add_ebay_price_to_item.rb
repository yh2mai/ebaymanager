class AddEbayPriceToItem < ActiveRecord::Migration
  def change
    add_column :items, :ebayPrice, :float
  end
end
