require 'uri'
require 'net/http'
require 'net/https'

module ItemsHelper
  def getxmlelement(xml_body, path)
    require 'rexml/document'
    # extract event information
    doc = REXML::Document.new xml_body
    return REXML::XPath.first(doc, path).text
  end
  
  def getapiurl
    if current_user.test_mode
      return "https://api.sandbox.ebay.com/ws/api.dll"
    else
      return "https://api.ebay.com/ws/api.dll"
    end
  end
  
  def req_post_header(ebay_call_name)
    @user = current_user
    url = URI.parse(getapiurl)
    req = Net::HTTP::Post.new(url.path)
    req.add_field("X-EBAY-API-COMPATIBILITY-LEVEL", "423")
    req.add_field("X-EBAY-API-DEV-NAME", @user.devID)
    req.add_field("X-EBAY-API-APP-NAME", @user.appID)
    req.add_field("X-EBAY-API-CERT-NAME", @user.certID)
    req.add_field("X-EBAY-API-SITEID", "0")
    req.add_field("X-EBAY-API-CALL-NAME", ebay_call_name)
    return req
  end
  
  def revise_item(ebayID, quantity, price)
    @user = current_user
    req = req_post_header("ReviseFixedPriceItem")
    url = URI.parse(getapiurl)
    req.body = 
    '<?xml version="1.0" encoding="utf-8"?>'+
    '<ReviseFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">'+
    '<ErrorLanguage>en_US</ErrorLanguage>'+
    '<WarningLevel>High</WarningLevel>'+
    '<Item>'+
    '<ItemID>' + ebayID + '</ItemID>'+
    '<StartPrice>' + price.to_s + '</StartPrice>'+
    '<Quantity>' + quantity.to_s + '</Quantity>'+
    '</Item>'+
    '<RequesterCredentials>'+
    '<eBayAuthToken>' + @user.ebayToken + '</eBayAuthToken>'+
    '</RequesterCredentials>'+
    '<WarningLevel>High</WarningLevel>'+
    '</ReviseFixedPriceItemRequest>​?'

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = 0
    res = http.start do |http_runner|
      http_runner.request(req)
    end
  end
  
  def get_my_ebay_selling(entries_per_page, page)
    @user = current_user
    req = req_post_header("GetMyeBaySelling")
    url = URI.parse(getapiurl)
    req.body = 
    '<?xml version="1.0" encoding="utf-8"?>' + 
    '<GetMyeBaySellingRequest xmlns="urn:ebay:apis:eBLBaseComponents">' + 
    '<ActiveList>' + 
    '<Sort>ItemID</Sort>' + 
    '<Pagination><EntriesPerPage>' + entries_per_page + '</EntriesPerPage>' +
    '<PageNumber>' + page + '</PageNumber>' + 
    '</Pagination>' + 
    '</ActiveList>' +
    '<RequesterCredentials>' +
    '<eBayAuthToken>' + @user.ebayToken + '</eBayAuthToken>' + 
    '</RequesterCredentials>' + 
    '<WarningLevel>High</WarningLevel>' +
    '</GetMyeBaySellingRequest>​'
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = 0
    res = http.start do |http_runner|
      http_runner.request(req)
    end
    return res.body
  end
  
  def get_suggested_category(query)
    @user = current_user
    url = URI.parse(getapiurl)
    req = req_post_header("GetSuggestedCategories")
    req.body = 
    '<?xml version="1.0" encoding="utf-8"?>' + 
    '<GetSuggestedCategoriesRequest xmlns="urn:ebay:apis:eBLBaseComponents">' + 
    '<Query>' + query + '</Query>' +
    '<RequesterCredentials>' + 
    '<eBayAuthToken>' + @user.ebayToken + '</eBayAuthToken>' + 
    '</RequesterCredentials>' + 
    '<WarningLevel>High</WarningLevel>' + 
    '</GetSuggestedCategoriesRequest>​'
    
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = 0
    res = http.start do |http_runner|
      http_runner.request(req)
    end
    
    category = getxmlelement(res.body, "//CategoryID")
    return category
  end
    
  def get_description_images_part(images)
    images_part=''
    images.each { |image_url|
      images_part += '<img src="' + image_url + '" onMouseOver="document.getElementById(\'mainimg\').src=this.src">'
    }
    return images_part
  end
  
  def get_xml_images_part(images)
    galeries = '<GalleryURL>' + '<![CDATA[' + images.first + ']]>' + '</GalleryURL>'
    images.each { |image_url|
      galeries += '<PictureURL>' + '<![CDATA[' + image_url + ']]>' + '</PictureURL>'
    }
    
    return galeries
  end
  
  def get_description_recommendations_part(category,ebayID)
    recommendation = ''
    @user = current_user
    same_category_item = @user.items.select { |a| a.category == category && 
      a.SYWAvailability == true && a.ebayID != ebayID }
    num_recommendation = 5
    if(same_category_item.nil?)
      num_recommendation = 0
    elsif(!same_category_item.kind_of?(Array))
      same_category_item = [same_category_item]
      num_recommendation = 1
    elsif(same_category_item.count < 5)
      num_recommendation = same_category_item.count
    end

    if(num_recommendation > 0)
      for i in 0..num_recommendation - 1
        ind = rand(same_category_item.count)
        item = same_category_item[ind]
        if(item.SYWAvailability == true)
          ebay_link = 'http://www.ebay.com/itm/a/' + item.ebayID
          if(item.thumb_url.nil?)
            item.thumb_url = ""
            item.save
          end
          recommendation = recommendation + '      <a href="' + ebay_link + '" target="_blank"><img name="" src="' + item.thumb_url + '" width="160" height="160" alt="" /><br />' + item.ebayTitle + ' </a>
           <div><a href="#"><strong>' + item.ebayPrice.round(2).to_s + '</strong><span><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/buyitnow.jpg"/></span></a><span><a href="' + ebay_link + '" target="_blank"></a></span></div>
           
           <br>
           <br>'
        else
           i = i + 1
        end
        same_category_item.delete_at(ind)
      end
    end
    return recommendation
  end
  
  def construct_ebay_description(recommendation,title,images,images_part,description)
    @user = current_user
    # encoding: utf-8
    require 'cgi'
    description = CGI.unescape(description)
    $new_description = '
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<style type="text/css">
body {
	font-size: 12px; background:#E8E8E8
}
form{ margin:0; padding:0}
.inwie{overflow:hidden;width:354px;height:44px;}
.inbg{float:left;overflow:hidden;width:278px;height:44px;background:url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/07top3.jpg);}
.inword{background:transparent;height:25px; line-height:25px; margin-left:80px; margin-top:11px;width:200px;color:#666; border:none;}

.wlogo{width:716px; height:177px; float:left}
.waplori{ width:354px; height:177px; float:right; overflow:hidden}

#wapwid{ width:1070px; margin:0 auto;}

#wapwid2{ width:1070px; margin:0 auto; background: url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/midbg.jpg) repeat-x #FFF; padding-top:20px}

#wapwid3{ width:1020px; margin:0 auto; background: url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/titbg.jpg) repeat-x; padding-top:20px}


.menu{ background:url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/menu04.jpg); width:1000px; height:40px; overflow:hidden; line-height:40px; color: #FFFFFF; font-size:14px; font-family:Geneva, Arial, Helvetica, sans-serif; font-weight: 600; margin:0 auto; margin-bottom:30px}
.menu ul,.menu li{ margin:0; padding:0}
.menu ul{ margin-left:20px; float:left; width:950px; overflow:hidden}
.menu li{ list-style:none; float:left; background:url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/menu03.jpg) right no-repeat; padding-left:20px; padding-right:20px;}
.menu li a{color:#ffffff; text-decoration:none;
-webkit-transition: all 0.3s linear;
-moz-transition: all 0.3s linear;
-ms-transition: all 0.3s linear;
-o-transition: all 0.3s linear;
transition: all 0.3s linear;-webkit-font-smoothing: antialiased;}
.menu li a:hover{ color: #4DD9B9; text-decoration:underline;opacity:0.6}
.menu .noe{ background:none}

.fl_le{ float:left}
.fl_ri{ float: right}



.fl{float:left; width:200px;}


.fenle{ border-left:1px solid #000;border-right:1px solid #000; width:198px; text-align:left}

.fenle ul,.fenle li{ margin:0; padding:0; list-style:none; line-height:35px; font-family:Verdana, Geneva, sans-serif; }
.fenle ul li  a {color: #fff;text-decoration: none; font-size:13px; font-weight:bold ; display:block; background:url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/07_categbg.jpg);  padding-left:25px;width:173px; overflow:hidden;
-webkit-transition: all 0.3s linear;
-moz-transition: all 0.3s linear;
-ms-transition: all 0.3s linear;
-o-transition: all 0.3s linear;
transition: all 0.3s linear;-webkit-font-smoothing: antialiased;}



.fenle ul ul{ margin:0px}

.fenle ul ul li a{padding-left:25px;text-align:left; font-weight:normal; background:url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/07_categbg2.jpg); width:173px; color:#CCC; overflow:hidden}
.fenle ul li a:hover{color:#Fff;text-decoration: none; background:url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/07_categon.jpg); opacity:0.8}
.fenle ul ul li a:hover{color:#Fff;text-decoration: none; background:url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/07_categon2.jpg); opacity:0.8}


.ovha img {position:absolute;clip:rect(0px 0px 120px 0px)}

.hotbg{ background:url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/hotbg.jpg); width:200px; font-family:Geneva, Arial, Helvetica, sans-serif; font-size:12px }
.proma,.copyal{font-family:Geneva, Arial, Helvetica, sans-serif; font-size:12px}
.copyal{ color:#666;}
.proma td{ padding-top:15px}
.proma img{max-width:180px;max-height:180px}
.copyal a{ margin-right:10px;margin-left:10px}
.hotbg a,.proma a,.copyal a{ color:#666; text-decoration:none; font-size:12px;

-webkit-transition: all 0.3s linear;
-moz-transition: all 0.3s linear;
-ms-transition: all 0.3s linear;
-o-transition: all 0.3s linear;
transition: all 0.3s linear;-webkit-font-smoothing: antialiased;
}
.hotbg a:hover,.proma a:hover,.copyal a:hover{ color:#FF6600; text-decoration:underline;opacity:0.8}
.hotbg img,.proma img{ border:1px solid #dddddd; margin-bottom:8px; vertical-align: middle}
.hotbg img{ width:160px}
.hotbg img span{ width:auto}
.hotbg strong,.proma strong{ color:#CC0000; font-size:12px; line-height:22px; margin-right:10px}
.hotbg span img,.proma span img{ border:none; margin-top:5px; width:auto }

.clearfix:after{ clear:both; height:0; overflow: hidden; display:block; visibility:hidden; content:"."}
.rigwid{ width:800px;  float:right}

.titwid{width:800px; overflow:hidden}
.title{border-bottom:1px solid #ccc; border-right:1px solid #ccc; height:35px; width:35px; float:left}
.title img{ margin-top:8px}
.tit {float:left; border-bottom:1px solid #ccc; width:755px; overflow:hidden; text-align:center; height:35px;}
.tit h1{font-family:Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0; font-size:16px; line-height:35px;}

.desc{ padding-top:20px; padding-bottom:40px; font-family:Verdana, Geneva, sans-serif; width:800px; margin:0 auto; line-height:22px; font-family: Verdana, Geneva, sans-serif}

.chabgc{ padding-top:10px; padding-bottom:10px;  width:780px; margin:0 auto}


.Ntia{height:62px; line-height:50px; margin-right:10px;font-family:Geneva, Arial, Helvetica, sans-serif; font-size:14px; color:#ffffff; font-weight:bold;background:url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/changon.jpg);float:left;text-align:center; cursor: pointer; text-align:center; width:132px;-webkit-transition: all 0.3s linear;
-moz-transition: all 0.3s linear;
-ms-transition: all 0.3s linear;
-o-transition: all 0.3s linear;
transition: all 0.3s linear;-webkit-font-smoothing: antialiased;}
.Baik{height:62px;line-height:50px;font-family:Geneva, Arial, Helvetica, sans-serif;  margin-right:10px; font-size:14px;  color:#FFFFFF;float:left;text-align:center; font-weight:bold; cursor: pointer; width:132px;
opacity:0.6}
.change{height:62px; width:800px;background:url(http://i280.photobucket.com/albums/kk176/Stephy_Wang/changebg.jpg);}

.desbg{ width:800px;font-size:14px; font-family:Geneva, Arial, Helvetica, sans-serif; line-height:20px; color:#555555; text-align:left; overflow:hidden}

.recomt{background:#434343; width:800px; height:40px; line-height:40px; color:#fff; font-family:Verdana, Geneva, sans-serif}
.recomt h2{ margin:0; padding:0; font-size:18px; margin-left:15px; font-weight:bold}



.datuhi{ width:800px; overflow:hidden;padding-top:10px}
.datu{ width:800px; height:500px;overflow:hidden;}
.datu img{max-width:100%; max-height:500px}

.smatu{ width:95%;margin-left:15px; margin-bottom:15px;  margin-top:15px;}
.smatu img,.smatu a img{ border:0;margin-right:5px;cursor: pointer; margin-bottom:10px;
-webkit-transition: all 0.3s linear;
-moz-transition: all 0.3s linear;
-ms-transition: all 0.3s linear;
-o-transition: all 0.3s linear;
transition: all 0.3s linear;-webkit-font-smoothing: antialiased; width:70px; height:70px}
.smatu  a:hover img,.smatu a:hover img{opacity:0.6}

/* image gallery */
#x-tg											{ width:800px; overflow:hidden; }
#x-tg-main									{ width:800px; margin:0 auto}
#x-tg-main img							{ height:500px; max-width:720px;  }
#x-tg-thumbs								{}
#x-tg-thumbs, .x-th-title,
.x-th-mid{ width:800px; margin-top:15px}
#x-tg-thumbs .x-th-mid ul{
	width:800px; 
	margin:0px 8px;
	padding:0;
	list-style-type:none;
}
#x-tg-thumbs .x-th-mid ul li{ float:left}
#x-tg-thumbs .x-th-mid				{ background:transparent none; padding:0; overflow:hidden; }
#x-tg-thumbs .x-th-mid div			{ display:inline-block; margin:0; padding:0; }
#x-tg-thumbs .x-th-mid img			{ height:72px; width:72px;border:1px solid #BBB; margin:0 0 8px 5px; cursor:pointer; } 
#x-tg-thumbs .x-th-mid img.x-hide	{ display:none; } 
#x-tg-thumbs .x-cl{ clear:both; width:0; height:0; }



.hotbg a img, .leftim a img {-webkit-transition: all 0.3s linear;
-moz-transition: all 0.3s linear;
-ms-transition: all 0.3s linear;
-o-transition: all 0.3s linear;
transition: all 0.3s linear;-webkit-font-smoothing: antialiased;}
.leftim a:hover img {opacity:0.6}
.hotbg  a:hover img,.proma a:hover img{opacity:0.9}

</style>
</head>

<body>
<table width="1070" border="0" align="center" cellpadding="0" cellspacing="0" background="#FFFFFF">
  <tr>
    <td><div id="wapwid" style="height:177px;">
   <div class="wlogo"><a href="' + @user.store_URL + '/" target="_blank"><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/07_top.jpg" border="0"  /></a></div> 
   <div class="waplori">
   <div style="height:111px; overflow:hidden"><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/07top2.jpg"  /></div>
    <div class="inwie">
          <form id="Search" method="get" name="Search" action="' + @user.store_URL + '/">
		  <div class="inbg">
           <input onblur="if(this.value==\'\')this.value=\'Search\';" onclick="if(this.value==\'Search\')this.value=\'\';" value="Search" name="_nkw"  class="inword"/>  </div>
		   <div style="float:left">
              <input name="submit" type="image" src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/07top4.jpg">
         </div>
        </form>
    </div>
     <div><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/07top5.jpg"  /></div>
 </div>
 </div>
 </td>
  </tr>
  <tr>
    <td>
    <div id="wapwid2" class="clearfix">
 <div class="menu">
 <div class="fl_le"><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/07_menu01.jpg"  /></div>
  <ul>
    <li><a href="' + @user.store_URL + '/" target="_blank">STORE HOME </a></li>
    <li><a href="' + @user.store_URL + '//_i.html?_sop=10&amp;rt=nc" target="_blank"> NEW ARRIVAL</a></li>
	<li><a href="' + @user.store_URL + '//_i.html?_sop=12&amp;rt=nc" target="_blank">HOT ITEMS</a></li> 
    <li><a href="#about">SHIPPING</a></li>
     <li><a href="http://my.ebay.com/ws/eBayISAPI.dll?AcceptSavedSeller&amp;sellerid=' + @user.store_ID + '&amp;ssPageName=STRK:MEFS:ADDSTR&amp;rt=nc" target="_blank">ADD TO FAVS</a></li>
      <li><a href="http://contact.ebay.com/ws/eBayISAPI.dll?ReturnUserEmail&amp;requested=' + @user.store_ID + '&amp;frm=3692&amp;iid=-1&amp;de=off&amp;redirect=0"> CONTACT US</a></li>
      <li class="noe"><a href="http://feedback.ebay.com/ws/eBayISAPI.dll?ViewFeedback&amp;userid=' + @user.store_ID + '" target="_blank">FEEDBACK</a></li>  
  </ul>
  <div class="fl_ri"><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/07_menu02.jpg"  /></div>
  </div>
  
  <div id="wapwid3" class="clearfix">

     <div class="fl">
  <div><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/07_categ.jpg" width="200" height="145" /></div>
   <div class="fenle">
  <div class="ovha">
 
  
     <ul>
    <li><a href="' + @user.store_URL + '" target="_blank">Store home</a></li>
    </ul>
     <!--------- Replace the text YOUR-USER-ID with your eBay user ID, replace "&domain=.com" to "&domain=.co.uk" or others if necessary (see what is the eBay site url you\'re using) ---------> 
<script type="text/javascript">var a = "SRC=";b="http://apps.kernelbd.com/ebay/dynamic-shop-categories-v2/store-menu.php?userid=' + @user.store_ID + '&platform=.com";document.write ("<script type=\'text/javascript\' "+a+b+">");document.write(\'</scr\'+\'ipt>\');</script>




   </div>
   </div> <div><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/07_categbot.jpg"/> </div>
   <div class="leftim">
     <p><a href="http://my.ebay.com/ws/eBayISAPI.dll?AcceptSavedSeller&amp;sellerid=' + @user.store_ID + '&amp;ssPageName=STRK:MEFS:ADDSTR&amp;rt=nc" target="_blank"><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/newslet.jpg" border="0" /></a></p>
     <p><a href="http://stores.ebay.com/' + @user.store_ID + '/_i.html?_sop=10&amp;rt=nc" target="_blank"><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/viewall.jpg" border="0" /></a></p>
      
      <p><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/paypay.jpg" /></p>
   </div>

   <div><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/recommad.jpg" width="200" height="40" /></div>
 <div class="hotbg">
  <table width="180" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td align="center">
      
      <br />
      ' + recommendation + '




</td>
    </tr>
  </table></div> 
  <div><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/recommbot.jpg"/></div>
     </div>
     
     
     <div class="rigwid">
      <div class="titwid" style="height:50px">
        <div class="title" align="center"><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/titico.jpg" /></div>
         <div class="tit">
         <h1> ' + title + ' </h1>         
         </div>
         </div>
     
     <div class="datuhi">
        <div class="datu" align="center"><img src="' + images.first + '" name="mainimg" id="mainimg"  /></div>
        <div class="smatu">' + 
        images_part + 
      '     </div>
      </div>
         <div class="recomt">
        <h2>DESCRIPTION</h2>

      </div>
      <div class="desc">' + description + '
</div>
      <div class="clearfix" style="margin-bottom:30px">
              <script language="JavaScript" type="text/javascript">
	
function g(o) {
	return document.getElementById (o);
}
function ts(n) {
	for(var i=1;i<=5;i++) {
		if (g(\'meit_\'+i)) 
		{
		g(\'meit_\'+i).className=\'Baik\';
		g(\'MTB_\'+i).style.display=\'none\';
		}
	}
	var e=g(\'meit_\'+n);
	e.className=\'Ntia\';
	g(\'MTB_\'+n).style.display=\'\';
	
}
    </script><a name="about" id="about"></a>
              <div class="change">
                <div class="Ntia" id="meit_1"  onmouseover="
		if (document.getElementById(\'meit_1\')) 
		{
		document.getElementById(\'meit_1\').className=\'Baik\';
		document.getElementById(\'MTB_1\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_2\')) 
		{
		document.getElementById(\'meit_2\').className=\'Baik\';
		document.getElementById(\'MTB_2\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_3\')) 
		{
		document.getElementById(\'meit_3\').className=\'Baik\';
		document.getElementById(\'MTB_3\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_4\')) 
		{
		document.getElementById(\'meit_4\').className=\'Baik\';
		document.getElementById(\'MTB_4\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_5\')) 
		{
		document.getElementById(\'meit_5\').className=\'Baik\';
		document.getElementById(\'MTB_5\').style.display=\'none\';
		}
	var e=document.getElementById(\'meit_1\');
	e.className=\'Ntia\';
	document.getElementById(\'MTB_1\').style.display=\'\';">SHIPPING</div>
                <div class="Baik" id="meit_2" onmouseover="
		if (document.getElementById(\'meit_1\')) 
		{
		document.getElementById(\'meit_1\').className=\'Baik\';
		document.getElementById(\'MTB_1\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_2\')) 
		{
		document.getElementById(\'meit_2\').className=\'Baik\';
		document.getElementById(\'MTB_2\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_3\')) 
		{
		document.getElementById(\'meit_3\').className=\'Baik\';
		document.getElementById(\'MTB_3\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_4\')) 
		{
		document.getElementById(\'meit_4\').className=\'Baik\';
		document.getElementById(\'MTB_4\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_5\')) 
		{
		document.getElementById(\'meit_5\').className=\'Baik\';
		document.getElementById(\'MTB_5\').style.display=\'none\';
		}
	var e=document.getElementById(\'meit_2\');
	e.className=\'Ntia\';
	document.getElementById(\'MTB_2\').style.display=\'\';">PAYMENT</div>
                <div class="Baik" id="meit_3" onmouseover="
		if (document.getElementById(\'meit_1\')) 
		{
		document.getElementById(\'meit_1\').className=\'Baik\';
		document.getElementById(\'MTB_1\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_2\')) 
		{
		document.getElementById(\'meit_2\').className=\'Baik\';
		document.getElementById(\'MTB_2\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_3\')) 
		{
		document.getElementById(\'meit_3\').className=\'Baik\';
		document.getElementById(\'MTB_3\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_4\')) 
		{
		document.getElementById(\'meit_4\').className=\'Baik\';
		document.getElementById(\'MTB_4\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_5\')) 
		{
		document.getElementById(\'meit_5\').className=\'Baik\';
		document.getElementById(\'MTB_5\').style.display=\'none\';
		}
	var e=document.getElementById(\'meit_3\');
	e.className=\'Ntia\';
	document.getElementById(\'MTB_3\').style.display=\'\';">RETURNS</div>
                <div class="Baik" id="meit_4" onmouseover="
		if (document.getElementById(\'meit_1\')) 
		{
		document.getElementById(\'meit_1\').className=\'Baik\';
		document.getElementById(\'MTB_1\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_2\')) 
		{
		document.getElementById(\'meit_2\').className=\'Baik\';
		document.getElementById(\'MTB_2\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_3\')) 
		{
		document.getElementById(\'meit_3\').className=\'Baik\';
		document.getElementById(\'MTB_3\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_4\')) 
		{
		document.getElementById(\'meit_4\').className=\'Baik\';
		document.getElementById(\'MTB_4\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_5\')) 
		{
		document.getElementById(\'meit_5\').className=\'Baik\';
		document.getElementById(\'MTB_5\').style.display=\'none\';
		}
	var e=document.getElementById(\'meit_4\');
	e.className=\'Ntia\';
	document.getElementById(\'MTB_4\').style.display=\'\';">FEEDBACK</div>
               <div class="Baik" id="meit_5" onmouseover="
		if (document.getElementById(\'meit_1\')) 
		{
		document.getElementById(\'meit_1\').className=\'Baik\';
		document.getElementById(\'MTB_1\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_2\')) 
		{
		document.getElementById(\'meit_2\').className=\'Baik\';
		document.getElementById(\'MTB_2\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_3\')) 
		{
		document.getElementById(\'meit_3\').className=\'Baik\';
		document.getElementById(\'MTB_3\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_4\')) 
		{
		document.getElementById(\'meit_4\').className=\'Baik\';
		document.getElementById(\'MTB_4\').style.display=\'none\';
		}
		if (document.getElementById(\'meit_5\')) 
		{
		document.getElementById(\'meit_5\').className=\'Baik\';
		document.getElementById(\'MTB_5\').style.display=\'none\';
		}
	var e=document.getElementById(\'meit_5\');
	e.className=\'Ntia\';
	document.getElementById(\'MTB_5\').style.display=\'\';"> ABOUT US</div>
              </div>
			  <div class="desbg">
          <div id="MTB_1" align="left">
                <div class="chabgc">
<p><strong>All of our products come with FREE Standard SHIPPING!</strong></p>
<p>Handling time on our orders is between 1-2 business days. We will ship your item out using the most efficient carrier to your area(USPS, UPS, Fedex, LaserShip, etc). Once it has been shipped out, you should be receiving it within 1 - 3 business days!</p>
<p>To ensure consistency, we will ship to your Paypal address, so please ensure it is up to date. We are not responsible for undeliverable addresses.</p>
<p>Currently, we only ship to physical addresses located within the 48 contiguous states of America.</p>
<p>P.O. boxes, APO/FPO addresses, Alaska and Hawaii are outside of our shipping zone.</p>
                </div>
        </div>
            <div  id="MTB_2" align="left" style="display:none;">
                <div class="chabgc">
             We Only Accept Paypal on Ebay. For specific requirements, please feel free to contact us.
                <img src="http://pics.ebaystatic.com/aw/pics/paypal/imgECheck.gif" />

                </div>
            </div>
            <div id="MTB_3" align="left" style="display:none;">
                <div class="chabgc">
                
 <p>1. We understand that sometimes an item may not turn out to be just what you need. Refund or replacement request is available within 2 weeks after parcel received &amp; in original condition and packing.</p>
<p>2. Please contact us before returning items or opening a return request and we will provide the shipping address. DO NOT send without communication. We can always resolve our customer\'s issues.</p>
                
              </div>
            </div>
            <div id="MTB_4"  align="left" style="display:none;">
                <div class="chabgc">
                <ol>
  <li>Feedback is important to all of us participating on the eBay community. We are always working hard for maintain high standards of excellence and strive for 100% customer satisfaction. So yourPOSITIVE FEEDBACK is very important.</li>
  <li>Here is an illustration showing how to leave the positive feedback, please follow the following step. Thank you so much. And welcome back to our store again.</li>
</ol>

            
                <img src="http://i1316.photobucket.com/albums/t612/45338878/niubi002/feed.jpg"/></div>
            </div>
			<div id="MTB_5"  align="left" style="display:none;">
                <div class="chabgc">
 <p><strong>ABOUT US</strong></p>
<p>We are always trying to deliver you the best experience of shopping with us.</p>

                </div>
            </div>
			
			
           </div></div>

     </div>
     
     
     </div>
<br />
<br />


 </div>
    
    </td>
  </tr>
  <tr>
    <td align="center" height="60"><img src="http://i280.photobucket.com/albums/kk176/Stephy_Wang/copybot.jpg" /></td>
  </tr>
</table>

</body>
'

    if current_user.email == 'torontoms@outlook.com' || current_user.email == 'cowenecommerce@yahoo.ca'
      # encoding: utf-8
    require 'cgi'
      $new_description = CGI.unescape($new_description)
    else
      $new_description = description
    end
    $new_description = "<![CDATA[" + $new_description + "]]>"
    return $new_description
  end

  def getBrand(title)
    brand = "does not apply"
    if(title.downcase.include?("craftsman"))
      brand = "Craftsman"
    elsif(title.downcase.include?("kenmore"))
      brand = "Kenmore"
    elsif(title.downcase.include?("hamilton beach"))
      brand = "Hamilton Beach"
    elsif(title.downcase.include?("milwaukee"))
      brand = "Milwaukee"
    elsif(title.downcase.include?("coleman"))
      brand = "Coleman"
    elsif(title.downcase.include?("schwinn"))
      brand = "Schwinn"
    elsif(title.downcase.include?("conair"))
      brand= "Conair"
    elsif(title.downcase.include?("lego"))
      brand = "LEGO"
    elsif(title.downcase.include?("essential home"))
      brand = "Essential Home"
    elsif(title.downcase.include?("brentwood"))
      brand = "Brentwood"
    elsif(title.downcase.include?("kitchenaid"))
      brand = "KitchenAid"
    end
    
    return brand
  end
  
  def add_item_call(description, price, title, images, sywid, sywprice,sywavailable,model_num)
    description = description.force_encoding("UTF-8")
    url = URI.parse(getapiurl)
    
    # Get the category
    category =  '111422' # change it to get_suggested_category(title)
    if !current_user.test_mode
      category = get_suggested_category(title)
    end

    @user = current_user
    req = req_post_header("AddFixedPriceItem")
    
    # Get the images part in description and xml
    images_part = get_description_images_part(images)

    galeries = get_xml_images_part(images)

    # Get the recommendation part in the description
    recommendation = get_description_recommendations_part(category,"")
    
    quantity = 0
    if sywavailable
      quantity = @user.numStock
    end
    
    $new_description = construct_ebay_description(recommendation,title,images,
      images_part,description)

ebaytitle = title
if current_user.test_mode
  ebaytitle = rand(1000).to_s
end 

brand = getBrand(title)

    req.body = 
    '<?xml version="1.0" encoding="utf-8"?>
    <AddFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
    <ErrorLanguage>en_US</ErrorLanguage>
    <WarningLevel>High</WarningLevel>
    <Item>
    <AutoPay>true</AutoPay>
    <Title>' + ebaytitle + '</Title>
    <Description>' + $new_description + '</Description>
    <PrimaryCategory><CategoryID>' + category + '</CategoryID>
    </PrimaryCategory>
    <StartPrice>' + price.to_s + '</StartPrice>
    <CategoryMappingAllowed>true</CategoryMappingAllowed>
    <ConditionID>1000</ConditionID>
    <Country>US</Country>
    <Currency>USD</Currency>
    <DispatchTimeMax>2</DispatchTimeMax>
    <ListingDuration>GTC</ListingDuration>
    <ListingType>FixedPriceItem</ListingType>
    <PaymentMethods>PayPal</PaymentMethods>
    <PaymentMethods>PayPalCredit</PaymentMethods>
    <PaymentMethods>CreditCard</PaymentMethods>
    <PayPalEmailAddress>' + @user.paypal + '</PayPalEmailAddress>
    <PictureDetails><GalleryType>Gallery</GalleryType>' + galeries +
    '</PictureDetails>
    <PostalCode>95125</PostalCode>
    <ItemSpecifics>
       <NameValueList>
          <Name>Brand</Name>
          <Value>' + brand + '</Value>
       </NameValueList>
       <NameValueList>
          <Name>MPN</Name>
          <Value>' + model_num + '</Value>
       </NameValueList>
    </ItemSpecifics>
    <ProductListingDetails>
    <UPC>does not apply</UPC>
    <IncludeStockPhotoURL>true</IncludeStockPhotoURL>
    <IncludePrefilledItemInformation>true</IncludePrefilledItemInformation>
    <UseFirstProduct>true</UseFirstProduct>
    <UseStockPhotoURLAsGallery>true</UseStockPhotoURLAsGallery>
    <ReturnSearchResultOnDuplicates>true</ReturnSearchResultOnDuplicates>
    </ProductListingDetails>
    <Quantity>' + quantity.to_s + '</Quantity>
    <ReturnPolicy><ReturnsAcceptedOption>ReturnsAccepted</ReturnsAcceptedOption>
    <RefundOption>MoneyBack</RefundOption>
    <ReturnsWithinOption>Days_30</ReturnsWithinOption>
    <Description>If you are not satisfied, return the item for refund.10% restocking fee may apply.</Description>
    <ShippingCostPaidByOption>Buyer</ShippingCostPaidByOption>
    </ReturnPolicy>
    <ShippingDetails><ShippingType>Flat</ShippingType>
    <ShippingServiceOptions><ShippingServicePriority>1</ShippingServicePriority>
    <ShippingService>ShippingMethodStandard</ShippingService>
    <FreeShipping>true</FreeShipping>
    <ShippingServiceAdditionalCost>0.00</ShippingServiceAdditionalCost>
    </ShippingServiceOptions>
    </ShippingDetails>
    <Site>US</Site>
    </Item>
    <RequesterCredentials>
    <eBayAuthToken>' + @user.ebayToken + '</eBayAuthToken>
    </RequesterCredentials>
    <WarningLevel>High</WarningLevel>
    </AddFixedPriceItemRequest>​'

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = 0
    if @user.currentEBayTitle == ebaytitle
      return false
    else
      @user.currentEBayTitle = ebaytitle
      @user.save
    end
    res = http.start do |http_runner|
      http_runner.request(req)
    end
    resbody = res.body
    puts resbody
    response = getxmlelement(resbody, "*/Ack")

    if response != 'Failure'
      ebayid = getxmlelement(resbody, "*/ItemID")
      @item = Item.new(SYWID: sywid, 
      SYWPrice: sywprice,
      SYWAvailability: true,
      ebayID: ebayid,
      ebayTitle: ebaytitle,
      quantity: @user.numStock,
      ebayPrice: price,
      fixed_price: 0,
      user_id: current_user.id,
      category: category,
      thumb_url: images[0])
      if @item.save
        return true
      else
        return false
      end
    else
      return false
    end
  end

  def update_description_call(title,ebay_description, ebayID, img_urls,model_num,item)
    req = req_post_header("ReviseFixedPriceItem")
    # Get the images part in description and xml
    images_part = get_description_images_part(img_urls)
    galeries = get_xml_images_part(img_urls)
    
    # update thumb_url
    item.thumb_url = img_urls[0]
    item.save
    
    # Get the recommendation part in the description
    recommendation = get_description_recommendations_part(item.category, ebayID)
    
    $new_description = construct_ebay_description(recommendation,title,
    img_urls,images_part,ebay_description)
    
    if(model_num.nil? || model_num.empty?)
      model_num = "does not apply"
    end
    
    brand = getBrand(title)
    
    url = URI.parse(getapiurl)
    req.body = 
    '<?xml version="1.0" encoding="utf-8"?>'+
    '<ReviseFixedPriceItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">'+
    '<ErrorLanguage>en_US</ErrorLanguage>'+
    '<WarningLevel>High</WarningLevel>'+
    '<Item>'+
    '<ItemID>' + ebayID + '</ItemID>'+
    '<Title>' + title + '</Title>' +
    '<Description>' + $new_description + '</Description>' +
    '<PictureDetails><GalleryType>Gallery</GalleryType>' + galeries + '</PictureDetails>' +
    '<ItemSpecifics>
      <NameValueList>
        <Name>Brand</Name>
        <Value>' + brand + '</Value>
      </NameValueList>
      <NameValueList>
        <Name>MPN</Name>
        <Value>' + model_num + '</Value>
      </NameValueList>
    </ItemSpecifics>' +
    '</Item>'+
    '<RequesterCredentials>'+
    '<eBayAuthToken>' + @user.ebayToken + '</eBayAuthToken>'+
    '</RequesterCredentials>'+
    '<WarningLevel>High</WarningLevel>'+
    '</ReviseFixedPriceItemRequest>​?'
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = 0
    res = http.start do |http_runner|
      http_runner.request(req)
    end
    item = Item.find_by_ebayID(ebayID)
    item.ebayTitle = title
    item.save
    puts res.body
  end
  
  def get_suggested_price(price)
    profit = current_user.profit
    if(current_user.email == 'markmai88838@hotmail.com' ||
      current_user.email == 'torontoms@outlook.com' ||
      current_user.email == 'myh0209@gmail.com')
      return (profit + 0.3 + price) / 0.863
    else
      return (profit + 0.3 + 1.07 * price) / 0.863
    end
  end
  
  def getstringbetweentags(body, tag1, tag2)
    tag1_index = body.index(tag1)
    if(!tag1_index.nil?)
      tag1_index += tag1.length
      tag2_index = body.index(tag2, tag1_index)
      if(!tag2_index.nil?)
        return body[tag1_index..tag2_index - 1]
      end
    end
    return ""
  end
  
  def getallstringbetweentags(body, tag1, tag2)
    ret = []
    tag1_index = body.index(tag1, 0)
    while !tag1_index.nil? do
      tag1_index += tag1.length
      tag2_index = body.index(tag2, tag1_index)
      if(!tag2_index.nil?)
        ret << body[tag1_index .. tag2_index - 1]
      else
        break
      end
      tag1_index = body.index(tag1, tag2_index)
    end
    return ret
  end
  
end
