class ItemsController < ApplicationController
  include ItemsHelper
  def add
    @item = Item.new(SYWID: params[:SYWID], 
    SYWPrice: params[:SYWPrice],
    SYWAvailability: params[:SYWAvailability],
    ebayID: params[:ebayID],
    ebayTitle: params[:ebayTitle],
    quantity: params[:quantity],
    ebayPrice: params[:ebayPrice],
    fixed_price: params[:fixed_price],
    user_id: params[:user_id],
    category: params[:category],
    thumb_url: params[:thumb_url],
    unavailable_date: params[:unavailable_date])
    if(!Item.find_by_ebayID(params[:ebayID]))
      if(@item.save)
        flash[:success] = "Item is added"
      else
        flash[:danger] = "Item is not added"
      end
    end
    render 'new'
  end
  
  def add_from_SYW
    render 'new_syw'
  end
  
  def add_item_from_torontoms
    alfred_items = User.fourth.items
    craftsman_items = alfred_items.select{ |item| !item.ebayTitle.nil? && (item.ebayTitle.include? "Craftsman") }
    number_of_craftsman_items = craftsman_items.count
#    for i in 794..1000
      for i in 0..number_of_craftsman_items
        begin
          item = craftsman_items[i]
          if !item.SYWID.nil?
            add_item_with_syw_title_id("", item.SYWID)
          end
        rescue
          next
        end
      end
    redirect_to items_url
  end
  
  def destroy 
    ebayID = params[:item][:ebayID]
    @item = Item.find_by_ebayID(ebayID)
    @item.delete
    redirect_to items_url
  end
  
  def add_list_from_SYW
    render 'new_list_syw'
  end
  
  def query
    @query = params[:query]
    @seller = params[:seller]
    @brand = params[:brand]
    @min_price = params[:minprice]
    @max_price = params[:maxprice]
    @count = params[:count]
    @user = current_user
    if( @count.nil? )
      url = 'https://platform.shopyourway.com/products/search?q=' + @query + '&limit=50' + 
      '&filter=sellers:' + @seller + ';brands:' + @brand + ';price:' + @min_price + '-' + 
      @max_price +';freeShipping:1' + 
      '&token=' + @user.SYWToken + '&hash=' + @user.SYWHash

      require "open-uri" 
      #如果有GET请求参数直接写在URI地址中
      html_response = nil 
      open(url) do |http|  
        html_response = http.read  
      end

      @count = html_response.scan(/"count":[\d]*/).first[8..-1]
        
      @syw_ids = []
      if !@count.nil?
        syw_items = html_response.scan(/{"id"[^}]*}/)

        syw_items.each do |sywitem|
          syw_id = sywitem.scan(/"id":[\d]*/).first[5..-1]
          @syw_ids << syw_id
        end
      else
        @count = '0'
      end
    else
      @count = @count.to_i
      page = 0
      index = 0
      begin
        url = 'https://platform.shopyourway.com/products/search?q=' + @query + '&limit=50' + 
        '&filter=sellers:' + @seller + ';brands:' + @brand + ';price:' + @min_price + '-' + @max_price +';freeShipping:1' + 
        '&token=' + @user.SYWToken + '&hash=' + @user.SYWHash + '&page=' + page.to_s


        require "open-uri" 
        #如果有GET请求参数直接写在URI地址中
        html_response = nil 
        open(url) do |http|  
          html_response = http.read  
        end
        
        syw_items = html_response.scan(/{"id"[^}]*}/)
        syw_items.each do |sywitem|
          begin
            index+=1
            syw_id = sywitem.scan(/"id":[\d]*/).first[5..-1]
            add_item_with_syw_title_id('',syw_id)
          rescue
            next
          end
          puts "printing " + index.to_s + "th item"
        end
        page += 1
      end while page*50 <=@count
    end
    render 'new_list_syw'
  end
  
  def add_item
    sywid = params[:syw_id]
    title = params[:title]
    sywids = params[:syw_ids]
    if(sywids.empty?)
      add_item_with_syw_title_id(title,sywid)
    else
      sywids_array = sywids.split(",")
      sywids_array.each do |a_sywid|
        add_item_with_syw_title_id("",a_sywid)
      end
    end
    render 'new_syw'
  end
  
  def add_item_with_syw_title_id(title, sywid)
    @user = current_user
    @items = @user.items
    if(@items.find_by_SYWID(sywid))
      flash.now[:danger] = 'Item already added'
    else
      require 'open-uri'
      url = "http://sandbox.shopyourway.com/a/" + sywid
      body=open(url, 
      "Connection"=>"keep-alive", "User-Agent"=>"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36",
      :read_timeout=>nil).read
      
      if(body.include? 'product-variations')
        flash.now[:danger] = 'Item contains variation'
        return
      end
      
      # get the images url
      img_urls = get_img_urls(body)
      
      # get the price
      price = get_syw_price(body)
      
      if(price == 0)
        flash.now[:danger] = 'Cannot fetch from Shopyourway'
        return
      end
        
      # Get default title
      if(title.empty?)
        title = get_syw_title(body)
      end
      if(@items.find{|item| !item.ebayTitle.nil? && (item.ebayTitle.downcase.include? title.downcase)})
        flash.now[:danger] = 'Item already added'
        return
      end
      
      # If the title include a violated brand, return it
      if( (title.downcase.include? 'dewalt') ||
        (title.downcase.include? 'as seen on tv') ||
        (title.downcase.include? 'instyler') ||
        (title.downcase.include? 'dcd710s2') ||
        (title.downcase.include? 'oxo') ||
        (title.downcase.include? 'waterpik') ||
        (title.downcase.include? 'ab rocket') ||
        (title.downcase.include? 'slendertone') ||
        (title.downcase.include? 'nutribullet' ) ||
        (title.downcase.include? 'the black & decker') ||
        (title.downcase.include? 'spyderco') ||
        (title.downcase.include? 'sharkninja'))
        flash.now[:danger] = 'Item brand is violated!'
        return
      end
      
      # Get the model number
      model_num = get_model_num(body)

      # Get description displayed on ebay
      ebay_description = get_ebay_description(body)
      
      # Get syw availability
      sywavailable = true
      if body.index('out of stock')
        sywavailable = false
      end

      success = add_item_call(ebay_description, get_suggested_price(price), title, img_urls,
      sywid, price, sywavailable, model_num)
      
      if success
        flash.now[:success] = "Item is added"
      else
        flash.now[:danger] = "Fail to insert item " + sywid
      end
    end
  end
  
  def get_img_urls(body)
    img_urls = []
    image_tag_1 = "data-full-size-image-url=\""
    image_tag_2 = "getImage?url=http"
    image_tag_index = 0
    begin
      image_tag_index = body.index(image_tag_1, image_tag_index)
      if(!image_tag_index.nil?)
        image_tag_index = body.index(image_tag_2, image_tag_index) - 4
        if(!image_tag_index.nil?)
          image_tag_index += image_tag_2.length
          end_index = body.index("&", image_tag_index)
          url = body[image_tag_index..end_index - 1]
          require 'cgi'
          url = CGI.unescape(url)
          img_urls << url
          image_tag_index = end_index
        end
      end
    end while !image_tag_index.nil?
    
    return img_urls
  end
  
  def get_model_num(body) 
    model_num_str = body.scan(/Model# [^ ]*/)

    if(!model_num_str.nil? && !model_num_str.empty?)
      return model_num_str.first[7..-1]
    else
      return 'does not apply'
    end
  end
  
  def get_syw_price(body) 
    price = ""
    price_tag = "itemprop='price'>"
    price_index = body.index(price_tag)
    if(!price_index.nil?)
      price_index += price_tag.length
      begin
        c=body[price_index]
        if((c>='0' && c<='9'))
          break
        else
          price_index+=1
        end
      end while(true)
      begin
        c = body[price_index]
        if((c >= '0' && c <= '9') || c == '.')
          price = price + c
          price_index += 1
        else
          break
        end
      end while(true)
    end
    price = price.to_f

    return price
  end
  
  def get_ebay_description(body)
    # get video url
    video_url = ""
    media_tag_index = body.index("product-media-video\"")
    if(!media_tag_index.nil?)
      start_index = body.index("data-url", media_tag_index)
      if(body[start_index + 10, 4] == "http")
        start_index = start_index + 10
        end_index = body.index("\"", start_index)
        video_url = body[start_index..end_index - 1]
      end
    end
    
    # Get description and specification
    description = ""
    full_description = getstringbetweentags(body, "<div class=\"full-description hidden\">","</div>")
    short_description = getstringbetweentags(body, "itemprop=\"description\">", "</div>")
    if(full_description.length > short_description.length)
      description = full_description
    else
      description = short_description
    end
    specification = getstringbetweentags(body, "<table>","</table>")
    specification = "<table>" + specification + "</table>"
    
    # Get description displayed on ebay
    ebay_description = ""
    if(video_url.empty?)
      ebay_description = description + "<br>" + specification
    else
      ebay_description = "<a href=\"" + video_url + "\">Videos and Product Demos Link</a>" + 
      "<br>" + description + "<br>" + specification
    end
    
    require 'cgi'
      ebay_description = CGI.unescape(ebay_description)
    
    return ebay_description
  end
  
  def get_syw_title(body)
    title = getstringbetweentags(body, "<title>", " |")

    tag = current_user.Tag
    if(current_user.email == "cowenecommerce@yahoo.ca")
      keywords = getallstringbetweentags(body, "<span itemprop=\"title\">", "</span>")
      title += " /"
      for keyword in keywords.reverse
        if !keyword.nil?
          title += (" " + keyword)
        end
      end
    end
    if(title.length > 80 - tag.length)
      reverse_title = title.reverse
      space_index = reverse_title.index(" ", -(80-tag.length))
      length = title.length
      title = title[0, length - 1 - space_index] + tag
    else
      title = title + tag
    end
    return title
  end
  
  def index
    if(!logged_in?)
      redirect_to root_url
    else
      @user = current_user
      @all_items = @user.items
      @items = @user.items.reorder("\"ebayID\"").paginate(page: params[:page])
    end
  end
  
  def update_price
    Thread.start { back_ground_fetch }
    redirect_to items_url
  end
  
  def back_ground_fetch
    require 'rexml/document'
    @user = current_user
    @user.current_num = 0
    @user.save
    get_total_entries = get_my_ebay_selling("1","1")

    # extract event information
    doc = REXML::Document.new get_total_entries
    
    num_items = 0
    REXML::XPath.each(doc, "//TotalNumberOfEntries") {
      |element| num_items = element.text.to_i
    }

    $i = 1
    $num_pages = num_items / 200 + 1
    begin
      $xml_data = get_my_ebay_selling("200", $i.to_s)

      doc = REXML::Document.new $xml_data
      items = []
      REXML::XPath.each( doc, "//Item") {
        |element|
        user_items = @user.items
        ebayid = element.elements["ItemID"].text
        price = element.elements["BuyItNowPrice"].text.to_f
        quantity = element.elements["QuantityAvailable"].text.to_i
        if(item = user_items.find_by_ebayID(ebayid))
          if((item.ebayPrice - price).abs > 0.01 || item.quantity != quantity)
            item.ebayPrice = price
            item.quantity = quantity
            item.save
          end
          items << item
        else
          item = Item.new(ebayID:ebayid, ebayPrice:price, quantity: quantity,
          SYWPrice: 0, SYWAvailability: true, fixed_price: 0, user_id: @user.id)
          item.save
        end
      }

      for i in 0..1
        begin
        group_items = items.slice(i * 100, 100)
        @SYWURL = 'https://platform.shopyourway.com/products/get?ids='
        group_items.each do |item|
          if !item.SYWID.nil?
            @SYWURL += (item.SYWID + ",")
          end
        end
        @SYWURL += "&token=" + @user.SYWToken + "&hash=" + @user.SYWHash

        require "open-uri" 
        #如果有GET请求参数直接写在URI地址中
        html_response = nil 
        open(@SYWURL) do |http|  
          html_response = http.read  
        end

        syw_items = html_response.scan(/{"id"[^}]*}/)

        syw_items.each do |sywitem|
          syw_id = sywitem.scan(/"id":[\d]*/).first[5..-1]
          availability_str = sywitem.scan(/"availability":"[\w]*"/).first[16..-2]
          availability = true
          if(availability_str == "unavailable" || availability_str == "unknown")
            availability = false
          else
            availability = true
          end
          syw_price_str = sywitem.scan(/"price":[\d]*.[\d]*/).first
          syw_price = 0 
          if(!syw_price_str.nil?)
            syw_price = syw_price_str[8..-1].to_f
          end
          
          if(syw_price == 0)
            availability = false
          end
          
          if(syw_price < 35)
            syw_price += 6.5
          end
          
          user_items = current_user.items
          if(item = user_items.find_by_SYWID(syw_id))
            if(item.SYWAvailability != availability || (item.SYWPrice - syw_price).abs > 0.01)
              item.SYWAvailability = availability
              item.SYWPrice = syw_price
              item.save
            end
            suggested_price = get_suggested_price(item.SYWPrice)
            
            if(!item.SYWAvailability || item.SYWPrice < 0.5 || 
              (!item.unavailable_date.empty? && Date.parse(item.unavailable_date) > Date.today))
              if(item.quantity != 0)
                revise_item(item.ebayID, 0, suggested_price)
              end
            else
              update = false
              if(item.quantity != @user.numStock)
                update = true
              end
              if(item.fixed_price > 0.1 && item.fixed_price > suggested_price)
                suggested_price = item.fixed_price
              end
              if((suggested_price - item.ebayPrice).abs > 1 || update)
                revise_item(item.ebayID, @user.numStock, suggested_price)
                item.ebayPrice = suggested_price
                item.quantity = @user.numStock
                item.save
              end
            end
          end
        end
        rescue
          next
        end
      end
      $i += 1
      @user.current_num = ($i - 1)*200
      @user.save
    end while $i <= $num_pages
  end

  def clean
    require 'rexml/document'
    @user = current_user
    @user.current_num = 0
    @user.save
    get_total_entries = get_my_ebay_selling("1","1")

    # extract event information
    doc = REXML::Document.new get_total_entries
    
    num_items = 0
    REXML::XPath.each(doc, "//TotalNumberOfEntries") {
      |element| num_items = element.text.to_i
    }

    $i = 1
    $num_pages = num_items / 200 + 1
    items = []
    deleted_items = []
    begin
      $xml_data = get_my_ebay_selling("200", $i.to_s)

      doc = REXML::Document.new $xml_data
      items = []
      REXML::XPath.each( doc, "//Item") {
        |element|
        user_items = @user.items
        ebayid = element.elements["ItemID"].text
        price = element.elements["BuyItNowPrice"].text.to_f
        quantity = element.elements["QuantityAvailable"].text.to_i
        if(item = user_items.find_by_ebayID(ebayid))
          if((item.ebayPrice - price).abs > 0.01 || item.quantity != quantity)
            item.ebayPrice = price
            item.quantity = quantity
            item.save
          end
          items << item
        else
          item = Item.new(ebayID:ebayid, ebayPrice:10, quantity: 0,
          SYWPrice: 0, SYWAvailability: true, fixed_price: 0, user_id: @user.id)
          item.save
        end
      }
      $i+=1
    end while $i <= $num_pages
    
    user_items = @user.items
    for index in 0..user_items.count - 1
      user_item = user_items[index]
      not_found = true
      for j in 0..items.count - 1
        a_item = items[j]
        if(a_item.ebayID == user_item.ebayID)
          not_found = false
        end
      end
      if(not_found)
        deleted_items << user_item
      end
    end
    
    begin
      if(deleted_items.count)
        deleted_items[0].delete
        deleted_items.delete_at(0)
      end
    end while(deleted_items.count != 0)
    redirect_to items_url
  end
  
  def update_description
    Thread.start { background_update_description }
    redirect_to items_url
  end
  
  def background_update_description
    @items = current_user.items
    num_items = @items.count
    start = 0
    for i in start.. (num_items - 1)
      item = @items[i]
      sywid = item.SYWID
      if(sywid.nil? || sywid.empty?)
        next
      end
      ebayid = item.ebayID
      require 'net/http'
      uri = URI('http://www.shopyourway.com/a/' + sywid)
      res = Net::HTTP.get_response(uri)
      if res.code != "200"
        puts "item " + sywid + " is not available on syw"
        next
      end
      body = res.body

      # get the title
      title = get_syw_title(body)


      if(@items.find{|item| !item.ebayTitle.nil? && item.ebayID != ebayid && (item.ebayTitle.downcase.include? title.downcase)})
        puts item.ebayID + ' Item already added, same title'
        
        next
      end
        
      # get the images url
      img_urls = get_img_urls(body)
    
      # Get the model number
      model_num = get_model_num(body)

      # Get description displayed on ebay
      ebay_description = get_ebay_description(body)
    
      update_description_call(title,ebay_description, ebayid, img_urls,model_num,item)
      
      puts i.to_s + "item has been updated"
    end
  end
  
  def update
    @item = Item.find_by_ebayID(params[:item][:ebayID])
    if @item.update_attributes(item_params)
      flash[:success] = "Item updated"
      redirect_to items_path
    else
      render 'edit'
    end
  end
  
  private
  
    def item_params
      params.require(:item).permit(:ebayID, :SYWID, :fixed_price, :unavailable_date)
    end
end
