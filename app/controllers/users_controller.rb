class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :index, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: :destroy
  def index
    @users = User.paginate(page: params[:page])
  end
  
  def show 
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  
  def create
    @user = User.new(user_params)
    if @user.save
      log_in @user
      flash[:success] = "Welcome to the eBay Manager, " + @user.name + "!"
      redirect_to user_url(@user)
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      @user.violated_brands = @user.violated_brands.split(',')
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  def add_violated_brand
    violated_brand = params[:brand]
    violated_brands = current_user.violated_brands.split(",")
    violated_brands << violated_brand
    current_user.violated_brands = violated_brands.join(",")
    current_user.save
    redirect_to current_user
  end
  
  def destroy_brand
    violated_brand = params[:violated_brand]
    @user=current_user
    violated_brands = @user.violated_brands.split(",")
    violated_brands.delete(violated_brand)
    @user.violated_brands = violated_brands.join(",")
    @user.save
    redirect_to @user
  end
  
  private
  
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation,:devID, :appID,
                                   :certID, :ebayToken, :numStock, :SYWToken,
                                   :SYWHash, :profit, :paypal, :store_URL, :store_ID,
                                   :test_mode, :Tag)
    end
    
    # Before filters
    
    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    
    # Confirms the correct user
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    # Confirms an admin user
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
