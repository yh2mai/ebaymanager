class UnavailableDatesController < ApplicationController
  before_action :set_unavailable_date, only: [:show, :edit, :update, :destroy]

  # GET /unavailable_dates
  # GET /unavailable_dates.json
  def index
    @unavailable_dates = UnavailableDate.all
  end

  # GET /unavailable_dates/1
  # GET /unavailable_dates/1.json
  def show
  end

  # GET /unavailable_dates/new
  def new
    @unavailable_date = UnavailableDate.new
  end

  # GET /unavailable_dates/1/edit
  def edit
  end

  # POST /unavailable_dates
  # POST /unavailable_dates.json
  def create
    @unavailable_date = UnavailableDate.new(unavailable_date_params)

    respond_to do |format|
      if @unavailable_date.save
        format.html { redirect_to @unavailable_date, notice: 'Unavailable date was successfully created.' }
        format.json { render :show, status: :created, location: @unavailable_date }
      else
        format.html { render :new }
        format.json { render json: @unavailable_date.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /unavailable_dates/1
  # PATCH/PUT /unavailable_dates/1.json
  def update
    respond_to do |format|
      if @unavailable_date.update(unavailable_date_params)
        format.html { redirect_to @unavailable_date, notice: 'Unavailable date was successfully updated.' }
        format.json { render :show, status: :ok, location: @unavailable_date }
      else
        format.html { render :edit }
        format.json { render json: @unavailable_date.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /unavailable_dates/1
  # DELETE /unavailable_dates/1.json
  def destroy
    @unavailable_date.destroy
    respond_to do |format|
      format.html { redirect_to unavailable_dates_url, notice: 'Unavailable date was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_unavailable_date
      @unavailable_date = UnavailableDate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def unavailable_date_params
      params.require(:unavailable_date).permit(:ebayID, :unavailable_date)
    end
end
