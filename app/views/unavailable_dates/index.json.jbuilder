json.array!(@unavailable_dates) do |unavailable_date|
  json.extract! unavailable_date, :id, :ebayID, :unavailable_date
  json.url unavailable_date_url(unavailable_date, format: :json)
end
